pkglist = '/var/lib/misc/thishost/pkglist'

if FileTest.exist?(pkglist)
	Facter.add('pkglist') do
		setcode do
			open(pkglist).read().split("\n").join(' ')
		end
	end
end
