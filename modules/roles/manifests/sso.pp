class roles::sso {
	ssl::service { 'sso.debian.org':
		notify  => Exec['service apache2 reload'],
		key => true,
	}

	ensure_packages ( [
		"slapd",
		], {
		ensure => 'installed',
	})
	service { 'slapd':
		ensure  => running,
	}
	file { '/etc/ldap/slapd.d':
		ensure => absent,
		force  => true,
		notify  => Service['slapd'],
	}
	file { '/etc/ldap/slapd.conf':
		source => 'puppet:///modules/roles/sso/slapd.conf',
		notify  => Service['slapd'],
	}
	file { '/etc/ldap/slapd-ftmg.conf':
		source => 'puppet:///modules/roles/sso/slapd-ftmg.conf',
		notify  => Service['slapd'],
	}
	file { '/etc/default/slapd':
		source => 'puppet:///modules/roles/sso/default-slapd',
		notify  => Service['slapd'],
	}


	ssl::service { 'ftmg.sso.debian.org':
		notify  => Exec['service apache2 reload'],
		key => true,
	}
}
